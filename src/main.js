import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

import VueRouter from 'vue-router'

import App from './App.vue'

//import components
import ChatContainer from './components/chat-container.vue'
import GoalsContainer from './components/goals-container.vue'


const socket = new WebSocket('ws://api.motivated.tech');

Vue.prototype.$socket = socket

Vue.use(VueRouter)
Vue.use(Vuex)



const store = new Vuex.Store({
	plugins: [createPersistedState()],
	state: {
		user: {},
		users: [],
		messages: [],
		goals: [],
		showSettings: false
	},
	mutations: {
		setUser(state, user) {
			state.user = user
		},
		addUser(state, user) {
			state.users.push(user)
		},
		newMessage(state, msg) {
			state.messages.push(msg)
		},
		addGoal(state, goal) {
			state.goals.push(goal)
		},
		toggleGoal(state, id) {

			let index = state.goals.findIndex((g) => {
				return g.id == id
			})
			state.goals[index].status = !state.goals[index].status
		},
		toggleSettings(state) {
			state.showSettings = !state.showSettings
		}
	}
})


const routes = [
  { path: '/', component: ChatContainer},
  { path: '/goals', component: GoalsContainer }
]

const router = new VueRouter({
  routes
})

new Vue({
	router,
	store,
  el: '#app',
  render: h => h(App)
})
